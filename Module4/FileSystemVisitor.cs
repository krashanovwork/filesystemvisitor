﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Module4
{
    internal class FileSystemVisitor
    {
        private readonly string path;


        public event Action StartSearch = () =>  Console.WriteLine("Search started");

        public event Action EndSearch = () => Console.WriteLine("Search ended");

        public event Action<string> FileFound = fileName => Console.WriteLine($"Found file: '{fileName}'");

        public event Action<string> DirectoryFound = dirName => Console.WriteLine($"Found directory: '{dirName}'");


        public enum Flags { Abort, ExcludeFiles };
        public Flags Flag { get; set; }

        public FileSystemVisitor(string path) => this.path = path;

        //public FileSystemVisitor(string path, IsFiltered isFiltered): this(path) => 


        public IEnumerable<string> GetFileSystem()
        {
            DirectoryInfo dir = new DirectoryInfo(path);

            StartSearch.Invoke();

            if (Flag == Flags.Abort)
            {
                yield break;
            }
            if (Flag == Flags.ExcludeFiles)
            {
                foreach (FileInfo fileInfo in dir.GetFiles())
                {
                    FileFound.Invoke(fileInfo.Name);
                    yield return fileInfo.Name;
                }

                foreach (DirectoryInfo directoryInfo in dir.GetDirectories())
                {
                    DirectoryFound.Invoke(directoryInfo.Name);
                    yield return directoryInfo.Name;
                }
            }

            EndSearch.Invoke();
        }
    }
}
